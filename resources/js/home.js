var socket = io("https://wedo.myfeup.com:4000");
/*var applstinstances = new Vue({
    el: "#applstinstances",
    data: {
        dataitems: [
            {
                id: 11111111,
                points: [
                    { cellid: 9999, density: 10, value: 3.55 },
                    { cellid: 8888, density: 10, value: 3.55 },
                    { cellid: 7777, density: 10, value: 3.55 },
                    { cellid: 6666, density: 10, value: 3.55 },
                    { cellid: 5555, density: 10, value: 3.55 }
                ]
            }
        ],
        count: 0
    }
});*/
var applstinstances = new Vue({
    el: "#applstinstances",
    data: {
        dataitems: [],
        count: 0
    }
});
window.chartCounter = null;
window.chartInstances = null;
window.recordsCount = 0;

window.chartColors = {
    red: "rgb(255, 99, 132)",
    orange: "rgb(255, 159, 64)",
    yellow: "rgb(255, 205, 86)",
    green: "rgb(75, 192, 192)",
    blue: "rgb(54, 162, 235)",
    purple: "rgb(153, 102, 255)",
    grey: "rgb(201, 203, 207)"
};

var configCounter = {
    type: "bar",
    data: {
        labels: [0],
        datasets: [
            {
                label: "Total Instances Read (per second)",
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: [0],
                fill: false
            }
        ]
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: "Clustering Statistics"
        },
        tooltips: {
            mode: "index",
            intersect: false
        },
        hover: {
            mode: "nearest",
            intersect: true
        },
        scales: {
            xAxes: [
                {
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "Time"
                    },
                    beginAtZero: true
                }
            ],
            yAxes: [
                {
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "Value"
                    },
                    ticks: {
                        beginAtZero: true,
                        min: 0
                    }
                }
            ]
        }
    }
};

var configInstances = {
    type: "line",
    data: {
        labels: [0],
        datasets: [
            {
                label: "Clustering Size",
                fill: false,
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                data: [0]
            }
        ]
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: "Clustering Statistics"
        },
        tooltips: {
            mode: "index",
            intersect: false
        },
        hover: {
            mode: "nearest",
            intersect: true
        },
        scales: {
            xAxes: [
                {
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "Time"
                    }
                }
            ],
            yAxes: [
                {
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "Value"
                    },
                    ticks: {
                        beginAtZero: true,
                        min: 0
                    }
                }
            ]
        }
    }
};

$(function() {
    var ctxCounter = document.getElementById("chartCounter").getContext("2d");
    window.chartCounter = new Chart(ctxCounter, configCounter);

    var ctxInstances = document
        .getElementById("chartInstances")
        .getContext("2d");
    window.chartInstances = new Chart(ctxInstances, configInstances);
    startSocket();
});

function startSocket() {
    var page = "dashboard";
    socket.on("connect", function() {
        socket.emit("page", page);
    });
    socket.on("DashboardUpdate", function(data) {
        if (data.time) {
            if (data.time > 0) {
                addPoint(data);
                fillList(data);
            }
        }
    });

    socket.on("disconnect", function() {
        console.log("Disconnect " + page);
    });
}

function fillList(data) {
    var size = Object.keys(data.numbers).length;
    applstinstances.count = size;

    var newdataitems = [];
    for (var number in data.numbers) {
        var points = data.numbers[number];
        var item = { id: number, points: points };
        newdataitems.push(item);
    }
    applstinstances.dataitems = newdataitems;
    console.log("Numbers: " + size);
}

function addPoint(record) {
    if (parseInt(record.time) == 1) {
        recordsCount = 0;
        configCounter.data.labels = [];
        configInstances.data.labels = [];
        configCounter.data.datasets.forEach(function(dataset) {
            dataset.data = [];
        });
        chartCounter.update();
        configInstances.data.datasets.forEach(function(dataset) {
            dataset.data = [];
        });
        chartInstances.update();
    }

    configCounter.options.title.text =
        "Total Instances Read: " + record.counter;
    if (configCounter.data.datasets.length > 0) {
        configCounter.data.labels.push(record.time);
        configCounter.data.datasets[0].data.push(record.counter - recordsCount);
    }
    recordsCount = record.counter;
    if (configCounter.data.labels.length > 15) {
        configCounter.data.labels.shift();
        configCounter.data.datasets.forEach(function(dataset) {
            dataset.data.shift();
        });
    }
    chartCounter.update();

    if (configInstances.data.datasets.length > 0) {
        configInstances.data.labels.push(record.time);
        configInstances.data.datasets[0].data.push(record.clustering_size);
    }
    if (configInstances.data.labels.length > 12) {
        configInstances.data.labels.shift();
        configInstances.data.datasets.forEach(function(dataset) {
            dataset.data.shift();
        });
    }
    chartInstances.update();
}
