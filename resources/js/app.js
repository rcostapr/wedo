/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component("Lstintances", require("./components/ListInstances.vue").default);

//import Lstintances from "./components/ListInstances.vue";

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
/*
var applstinstances = new Vue({
    el: "#applstinstances",
    data: {
        items: [
            { id: 0, message: "Foo" },
            { id: 1, message: "Bar" }
        ]
    }
});
*/
/*
const app = new Vue({
    el: "#app"
});
*/

// Jquery
import $ from "jquery";
window.$ = window.jQuery = $;

import "jquery-ui/ui/widgets/datepicker.js";

// Chartjs
import Chart from "chart.js";

// Home.js
require("./home");
