<div class="block-flat col-md-12">
    <div class="content">
        <div class="container-fluid">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h4 class="m-0 font-weight-bold">Chart Instances</h4>
                </div>
                <div class="card-body">
                    <div class="chart-container md-12" id="chartcountdiv">
                        <canvas id="chartInstances" width="1200" height="300"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>