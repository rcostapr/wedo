@extends('layouts.app')
@section('title', $metatitle)
@section('description', $metadescr)
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    @include('partials.chartcounter')
                    @include('partials.chartinstances')
                    @include('partials.listinstances')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@foreach ($scripts as $script)
<script src="{{ $script }}"></script>
@endforeach
@endsection