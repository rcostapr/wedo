@extends('layouts.app')
@section('title', $metatitle)
@section('description', $metadescr)
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Documentation</div>

                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@foreach ($scripts as $script)
<script src="{{ $script }}"></script>
@endforeach
@endsection