var fs = require("fs");
var app = require("express")();
var https = require("https");
var server = https.createServer(
    {
        key: fs.readFileSync("./ssl.key"),
        cert: fs.readFileSync("./ssl.cert"),
        ca: fs.readFileSync("./ssl.ca"),
        requestCert: false,
        rejectUnauthorized: false
    },
    app
);
var io = require("socket.io")(server);
const bodyParser = require("body-parser");

const port = 4000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post("/notification", function(req, res) {
    JSON.stringify(req.body);
    console.log("receiving data ...");
    console.log("body is ", req.body);

    io.sockets.emit("DashboardUpdate", req.body);

    res.status(200).json({ data: "success" });
});

io.on("connection", function(socket) {
    console.log("a user connected");

    socket.on("disconnect", function() {
        console.log("user disconnected: " + socket);
    });
    socket.on("page", function(trackPage) {
        console.log(trackPage);

        switch (trackPage) {
            case "dashboard":
                io.sockets.emit("index", "New User on index.");
                break;
            case "index":
                io.sockets.emit("dashboard", "New User on dashboard.");
                break;
        }
    });
});

server.listen(port, function() {
    console.log(`Server running at https://localhost:${port}`);
});
