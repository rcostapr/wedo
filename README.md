# WeDo Machine Learning Project

# Create Authentication

-   composer require laravel/ui --dev
-   php artisan ui vue --auth

# Installing PHP version 7.2

The procedure to install PHP 7.2 on CentOS 7 or RHEL 7 is as follows:

-   Turn on EPEL repo, enter:
    \$ sudo yum -y install epel-release
-   Turn on Remi repo i.e.remi-php72:
    \$ sudo yum-config-manager --enable remi-php72
-   Refresh repository:
    \$ sudo yum update
-   Install php version 7.2, run:
    \$ sudo yum install php

# Cross Env

-   npm install --global cross-env
-   npm install --no-bin-links
-   npm run dev

# Node Server

## Install pm2

PM2 is a daemon process manager that will help you manage and keep your application online.

-   npm install pm2@latest -g
-   pm2 start index.js --name wedo --log ./logs/wedo-node-server.log
-   pm2 save

## Init Script

-   pm2 startup

## Commands

-   \$ pm2 restart app_name
-   \$ pm2 reload app_name
-   \$ pm2 stop app_name
-   \$ pm2 delete app_name
-   \$ pm2 [list|ls|status]

## Display logs

### To display logs in realtime:

-   \$ pm2 logs

### To dig in older logs:

-   \$ pm2 logs --lines 200

### Terminal Based Dashboard

Here is a realtime dashboard that fits directly into your terminal:

-   \$ pm2 monit

## Testing

-   curl https://localhost:4000/notification -X POST -H "Content-Type: application/json" -d '{"value":50}' -k

# Database

## Create Database

-   \$ php artisan migrate
