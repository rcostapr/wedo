import scrapy


class OmieSpider(scrapy.Spider):
    name = "omie"

    def start_requests(self):
        urls = [
            'http://m.omie.es/reports/?m=yes&lang=pt'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # return(response.css("div#containerTableIn table").get())
        # response.xpath('//div[@id="containerTableIn"]text()').get()
        header = response.xpath(
            '//div[@id="containerTableIn"]/table/th/text()').extract()
        yield {
            'header': header
        }
        items = []
        for rows in response.xpath('//div[@id="containerTableIn"]/table/tr'):
            row = rows.xpath('td/text()').extract()
            items.append(row)
        yield{
            'rows': items
        }
