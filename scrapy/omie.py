import json
import os
import scrapy
from scrapy.crawler import CrawlerProcess


class OmieSpider(scrapy.Spider):

    name = "omie"

    def start_requests(self):
        urls = [
            'http://m.omie.es/reports/?m=yes&lang=pt'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        # return(response.css("div#containerTableIn table").get())
        # response.xpath('//div[@id="containerTableIn"]text()').get()
        header = response.xpath(
            '//div[@id="containerTableIn"]/table/th/text()').extract()
        items = []
        for rows in response.xpath('//div[@id="containerTableIn"]/table/tr'):
            row = rows.xpath('td/text()').extract()
            items.append(row)
        date = response.xpath('//input[@id="selectdate"]/@value').getall()[0]
        yield{
            'date': date,
            'header': header,
            'rows': items
        }


process = CrawlerProcess(settings={
    'FEED_FORMAT': 'json',
    'FEED_URI': 'items.json'
})

if os.path.exists("items.json"):
    os.remove("items.json")

process.crawl(OmieSpider)
process.start()

data = ""
with open('items.json') as f:
    data = json.load(f)

print(data[0])
