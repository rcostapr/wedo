<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $scripts = [];
        $metatitle = "Dashboard :: Wedo Machine Learning";
        $metadescr = "Wedo Machine Learning Dashboard";
        return view('home', ["scripts" => $scripts, "metatitle" => $metatitle, "metadescr" => $metadescr]);
    }

    public function docs(){
        $scripts = [];
        $metatitle = "Documentation :: Wedo Machine Learning";
        $metadescr = "Wedo Machine Learning Documentation";
        return view('docs', ["scripts" => $scripts, "metatitle" => $metatitle, "metadescr" => $metadescr]);
    }
}
